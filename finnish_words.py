#!/usr/bin/python3

import os
import sys
import argparse
from util import *
from xml.dom import minidom
import palindrome_util
import voikko.inflect_word


def clean_word(word):
    return word.replace("-", "")


def load_finnish_words(word_set, filename):
    # https://github.com/voikko/corevoikko could be used instead for getting
    # the inflected versions of words
    # also https://github.com/indigane/voikkogen has related code
    print("Loading Finnish words")
    xml_doc = minidom.parse(filename)
    word_elements = xml_doc.getElementsByTagName("st")

    word_count = len(word_elements)
    index = 0
    for element in word_elements:
        if index % 100 == 0:
            print(str(index) + " / " + str(word_count))

        word_in_xml = str(element.firstChild.firstChild.data)
        word_cleaned = clean_word(word_in_xml)

        # add base form
        word_set.add(word_cleaned)

        # add inflected forms
        meaning_forms = voikko.inflect_word.generate_forms(word_in_xml)
        for meaning in meaning_forms:
            for forms in meaning.values():
                for form in forms:
                    word_set.add(clean_word(form))

        index += 1


def update_words():
    words_set = set()
    load_finnish_words(words_set, "finnish_first_names.xml")
    words = load_finnish_words(words_set, os.path.dirname(
        os.path.realpath(
            sys.argv[0])) + '\\..\\kotus-sanalista_v1\\kotus-sanalista_v1.xml')
    words = list(words_set)

    print("Sorting words")
    words.sort()
    print("Outputting")
    for word in words:
        output_line(word, False)
    print("Update words done")


def main():
    parser = argparse.ArgumentParser(description='Finnish word list')
    parser.add_argument(
        '-o', '--output', nargs='?', type=str, help='Output file')
    args = parser.parse_args()

    open_output(args.output, "w+")

    update_words()


if __name__ == "__main__":
    main()
