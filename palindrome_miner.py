#!/usr/bin/python3

from util import *
from palindrome_util import *


def find_multi_word_palindromes(words):
    miner = PalindromeMiner(1000, words, words, set())
    return miner.find_multi_word_palindromes_with_max_depth()


class PalindromeMiner:
    def __init__(self, max_word_count, root_words, words_set, palindromes_set):
        self.max_word_count = max_word_count
        self.root_words = root_words
        self.words_set = words_set
        self.palindromes_set = palindromes_set
        self.new_palindromes = []

    def find_multi_word_palindromes_with_max_depth(self):
        word_count = len(self.root_words)
        index = 0
        for word in self.root_words:
            self.find_palindromes_starting_with("", [], [], word, 0)
            index += 1
            if index % 100 == 0:
                print(str(index) + " / " + str(word_count))
        return self.new_palindromes

    def find_palindromes_starting_with(
        self,
        palindrome_start,
        left_word_chain,
        right_word_chain,
        part,
            indent_level):

        reversed_part = part[::-1]
        reversed_palindrome_start = palindrome_start[::-1]
        left_word_chain_length = len("".join(left_word_chain))
        right_word_chain_length = len("".join(right_word_chain))
        is_matching_left = left_word_chain_length >= right_word_chain_length
        is_matching_right = left_word_chain_length <= right_word_chain_length

        debug_log(
            indent_level,
            "searching1: " +
            palindrome_start + "..." +
            reversed_palindrome_start +
            " part: \"" + part +
            "\" left: " + str(left_word_chain) +
            " right: " + str(right_word_chain))

        for dictionary_word in self.words_set:
            if dictionary_word != part and \
                    not (dictionary_word in palindrome_start or
                         dictionary_word in reversed_palindrome_start):
                reversed_dictionary_word = dictionary_word[::-1]
                if is_matching_left and \
                        dictionary_word.endswith(reversed_part):
                    if part in self.words_set and not (
                            len(left_word_chain) > 0 and
                            (palindrome_start + part).
                            endswith(left_word_chain[-1])):
                        new_left_word_chain = left_word_chain.copy()
                        new_left_word_chain.append(part)
                    else:
                        new_left_word_chain = left_word_chain

                    new_right_word_chain = right_word_chain.copy()
                    new_right_word_chain.insert(0, dictionary_word)

                    self.find_palindromes(
                        palindrome_start,
                        new_left_word_chain,
                        new_right_word_chain,
                        part,
                        dictionary_word,
                        indent_level + 1)
                elif is_matching_right and len(right_word_chain) > 0 and \
                    dictionary_word.startswith(part) and \
                        (reversed_part + reversed_palindrome_start).\
                        startswith(right_word_chain[0]):
                    new_left_word_chain = left_word_chain.copy()
                    new_left_word_chain.append(dictionary_word)
                    self.find_palindromes(
                        palindrome_start,
                        new_left_word_chain,
                        right_word_chain,
                        dictionary_word,
                        reversed_part,
                        indent_level + 1)
                elif is_matching_left and \
                        part.startswith(reversed_dictionary_word):
                    if left_word_chain_length == right_word_chain_length and \
                            part in self.words_set:
                        new_left_word_chain = left_word_chain.copy()
                        new_left_word_chain.append(part)
                    else:
                        new_left_word_chain = left_word_chain
                    new_right_word_chain = right_word_chain.copy()
                    new_right_word_chain.insert(0, dictionary_word)
                    self.find_palindromes(
                        palindrome_start,
                        new_left_word_chain,
                        new_right_word_chain,
                        part,
                        dictionary_word,
                        indent_level + 1)
                elif is_matching_right and \
                    dictionary_word.startswith(reversed_part) and \
                    (reversed_part + reversed_palindrome_start).\
                        startswith(dictionary_word):
                    new_left_word_chain = left_word_chain.copy()
                    if part in self.words_set:
                        new_left_word_chain.append(part)
                        self.find_palindromes(
                            palindrome_start,
                            new_left_word_chain,
                            right_word_chain,
                            part,
                            reversed_part,
                            indent_level + 1)
                    else:
                        sub_part = part
                        found = True
                        while found:
                            found = False
                            for dic_word in self.words_set:
                                if sub_part.startswith(dic_word):
                                    new_left_word_chain.append(dic_word)
                                    sub_part = sub_part[len(dic_word):]
                                    found = True
                                    break
                        if len(sub_part) < len(part):
                            self.find_palindromes(
                                palindrome_start +
                                part[0:len(part) - len(sub_part)],
                                new_left_word_chain,
                                right_word_chain,
                                "",
                                sub_part[::-1],
                                indent_level + 1)

    def find_palindromes(
            self,
            palindrome_start,
            left_word_chain,
            right_word_chain,
            left_part,
            right_part,
            indent_level):

        debug_log(
            indent_level, "searching2: " + palindrome_start + left_part +
            "..." + right_part + palindrome_start[::-1] + "\" left: " +
            str(left_word_chain) + " right: " + str(right_word_chain))

        palindrome_candidate = \
            palindrome_start + left_part + right_part + palindrome_start[::-1]
        if is_character_palindrome(palindrome_candidate):
            # palindrome found
            found_result = " ".join(left_word_chain + right_word_chain)
            if found_result in self.palindromes_set:
                debug_log(indent_level, "found existing: " + found_result)
                print(".", end='')
            else:
                debug_log(indent_level, "found new: " + found_result)
                self.palindromes_set.add(found_result)
                output_line(found_result, True)
                self.new_palindromes.append(found_result)

            assert(
                "".join(left_word_chain + right_word_chain) ==
                palindrome_candidate)

        if len(left_word_chain) + len(right_word_chain) >= self.max_word_count:
            pass
        elif len(right_part) > len(left_part):
            # continue search
            part = right_part[0:(len(right_part) - len(left_part))][::-1]
            self.find_palindromes_starting_with(
                palindrome_start + left_part,
                left_word_chain,
                right_word_chain,
                part,
                indent_level + 1)
        elif len(left_part) > len(right_part):
            # continue search
            part = left_part[len(right_part):]
            self.find_palindromes_starting_with(
                palindrome_start + right_part[::-1],
                left_word_chain,
                right_word_chain,
                part,
                indent_level + 1)
