# palindrome-miner
Palindrome miner is an application to find and produce new palindromes. Currently the focus is on Finnish language.

## Palindrome categories

1. Center pieces.
    1. Individual words. Examples: "sadas", "äijiä", "saippuakauppias"
    2. Multi-word non-splittable. Examples: "ollapa pallo", "siestat seis", "alle satasella", "sinä ja jänis"
2. Splittable
    1. Word boundary in center: "tosi isot", "taksin niskat", "ei lääkärillä älliräkää lie"
    2. Repeated center pieces: "sadas sadas", "ollapa pallo"

## Palindrome mining
Palindromes can be mined using an algorithm that goes through a dictionary of allowed words and finds sequences that produce palindromes. The found palindromes are categorized and stored. The repeated center pieces are omitted as they can be derived from the other categories.

## Palindrome composition
New palindromes can be composed by using computer-aided palindrome composition (CAPC) described here. 

1. The user first selects a center piece palindrome to start composing on. The selection can be done by e.g. starting to type and application showing auto-complete options from database.
2. The user selects a splittable palindrome to append to the current one. This can be done again by typing and application offering to auto-complete. The application adds the matching text to the start of the palindrome while user types it to the end.
3. The step #2 is repeated as many times until the user is satisfied with the resulting palindrome.
4. The end result is optionally stored

### Example
1. Center piece: "sadas"
2. Add "taksin niskat" (word boundary in center) -> "taksin sadas niskat"
3. Add "tosi isot" (word boundary in center) -> "tosi taksin sadas niskat isot

Note that the application is not supposed to know the semantics of the words. Thus it is the user's reponsibility to create sequences that make sense or are amusing.
