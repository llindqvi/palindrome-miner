#!/usr/bin/python3

import os
import sys
import argparse
from util import *
from palindrome_util import *
from xml.dom import minidom
from palindrome_fragment import *
from palindrome_miner import *


def test():
    test_edit_distance()
    test_is_character_palindrome()
    test_is_splittable_palindrome()
    test_from_serialized()
    test_words_without_extra()
    test_palindrome_fragment_serialized()
    test_can_add_palindrome_inside()
    test_can_add_palindrome_around()
    test_as_word_boundaries()
    test_find_single_word_palindromes()
    test_find_single_word_fragments()
    test_find_multi_word_palindromes()
    test_palindrome_compatibility()
    test_mine_fragments()
    print("Tests passed")


def test_edit_distance():
    assert(palindrome_util.edit_distance("test", "test") == 0)
    assert(palindrome_util.edit_distance("temp", "test") == 2)
    assert(palindrome_util.edit_distance("te", "test") == 2)
    assert(palindrome_util.edit_distance("test", "es") == 2)
    assert(palindrome_util.edit_distance("test test", "test") == 5)


def get_fragment(text):
    max_extra_length = 4
    fragment = PalindromeFragment.from_serialized(text, max_extra_length)
    assert(text == fragment.serialized() or text == fragment.user_str())

    bracketless = text.replace("[", "").replace("]", "")
    test_fragment = PalindromeFragment.from_serialized(
        bracketless, max_extra_length)
    assert(fragment.serialized() == test_fragment.serialized())
    assert(fragment.user_str() == test_fragment.user_str())

    return fragment


def test_can_add_palindrome_inside():
    assert(get_fragment("alla").can_add_palindrome_inside() is False)
    assert(get_fragment("alla alla").can_add_palindrome_inside() is True)
    assert(get_fragment("alla alla alla").can_add_palindrome_inside() is False)


def test_can_add_palindrome_around():
    assert(get_fragment("alla").can_add_palindrome_around() is True)
    assert(get_fragment("alla alla").can_add_palindrome_inside() is True)
    assert(get_fragment("alla alla alla").can_add_palindrome_inside() is False)


def test_as_word_boundaries():
    whitespaceless, boundaries = \
        PalindromeFragment.as_word_boundaries("")
    assert(whitespaceless == "" and boundaries == [])

    whitespaceless, boundaries = \
        PalindromeFragment.as_word_boundaries("a")
    assert(whitespaceless == "a" and boundaries == [])

    whitespaceless, boundaries = \
        PalindromeFragment.as_word_boundaries("ab ba")
    assert(whitespaceless == "abba" and boundaries == [2])

    whitespaceless, boundaries = \
        PalindromeFragment.as_word_boundaries("ab bab ba")
    assert(whitespaceless == "abbabba" and boundaries == [2, 5])

    whitespaceless, boundaries = \
        PalindromeFragment.as_word_boundaries(
            "E.T. - taikina kakan iki Atte!")
    assert(whitespaceless == "ettaikinakakanikiatte" and
           boundaries == [2, 9, 14, 17])


def assert_from_serialized(text_with_brackets):
    text_no_brackets = text_with_brackets.replace("[", "").replace("]", "")
    assert(
        text_with_brackets == get_fragment(text_no_brackets).serialized())
    assert(
        text_with_brackets == get_fragment(text_with_brackets).serialized())


def test_from_serialized():
    assert_from_serialized("")
    assert_from_serialized("a")
    assert_from_serialized("nolon")
    assert_from_serialized("nolon nolon")
    assert_from_serialized("nolon nolon nolon")
    assert_from_serialized("unna anna annu")
    assert_from_serialized("[a]b")
    assert_from_serialized("[a]iro ori")
    assert_from_serialized("[ab]iro ori")
    assert_from_serialized("[abc]iro ori")
    assert_from_serialized("iro ori[a]")
    assert_from_serialized("iro ori[ba]")
    assert_from_serialized("ajot [ker]toja")
    assert_from_serialized("ajot [e]toja")
    assert_from_serialized("ii h[iki] hii")
    assert_from_serialized("onni [h]inno[issa]")
    assert_from_serialized("onni h[issi] hinno")
    assert_from_serialized("unia[su] ainu")
    assert_from_serialized("anas [a]sana")

    # recognition of inner and outer extras in the same text
    assert_from_serialized("sala [p]alas")
    assert_from_serialized("sala [p]alas[i]")
    assert_from_serialized("sala p[ala] palas[i]")
    assert_from_serialized("sala pala [p]ala palas[i]")
    assert_from_serialized("[an]anas [a]sana")
    assert_from_serialized("[k]akka[ra] akka")
    assert_from_serialized("akka [k]akka[ra]")

    # TODO extra spanning over words
    # assert(
    #    "[ab c]isi" == get_fragment("ab cisi").serialized())


def test_words_without_extra():
    assert(list(get_fragment("akka").words_without_extra()) ==
           ["akka"])
    assert(list(get_fragment("akka sakka").words_without_extra()) ==
           ["akka", "akka"])
    assert(list(get_fragment("akka siki sakka").words_without_extra()) ==
           ["akka", "s", "sakka"])
    assert(list(get_fragment("[k]akka[ra] akka").words_without_extra()) ==
           ["akka", "akka"])
    assert(list(get_fragment("akka [k]akka[ra]").words_without_extra()) ==
           ["akka", "akka"])


def test_palindrome_compatibility():
    assert(get_fragment("alla").try_add_inside(
        get_fragment("unna annu")) is None)

    assert(get_fragment("alla").try_add_outside(
        get_fragment("unna annu")).serialized() == "unna alla annu")

    assert(get_fragment("unna annu").try_add_inside(
        get_fragment("anna anna")).serialized() == "unna anna anna annu")

    assert(get_fragment("unna annu").try_add_outside(
        get_fragment("anna anna")).serialized() == "anna unna annu anna")

    assert(get_fragment("unna annu").try_add_outside(
        get_fragment("anna anna")).serialized() == "anna unna annu anna")

    assert(get_fragment("[a]iro ori").try_add_inside(
        get_fragment("ajon [sa]noja")).serialized() ==
        "[a]iro ajon [sa]noja ori")

    assert(get_fragment("[a]iro ori").try_add_inside(
        get_fragment("ajon [a]noja")).serialized() ==
        "[a]iro ajon [a]noja ori")

    assert(get_fragment("[a]iro ori").try_add_inside(
        get_fragment("anna")).serialized() == "[a]iro anna ori")

    assert(get_fragment("[a]iro ori").try_add_outside(
        get_fragment("ajon [a]noja")).serialized() == "ajon airo ori anoja")
    assert(get_fragment("ajon [a]noja").try_add_inside(
        get_fragment("[a]iro ori")).serialized() == "ajon airo ori anoja")

    assert(get_fragment("[a]iro ori").try_add_outside(
        get_fragment("ajon [a]noja")).serialized() == "ajon airo ori anoja")

    assert(get_fragment("aatam[i] mataa").try_add_inside(
        get_fragment("ah ha[i]")).serialized() == "aatami ah hai mataa")

    assert(get_fragment("aatam[i] mataa").try_add_outside(
        get_fragment("ah ha[i]")).serialized() == "ah aatam[i] mataa ha[i]")

    assert(get_fragment("[k]anuuna").try_add_outside(
        get_fragment("ihra [k]arhi")).serialized() == "ihra kanuuna karhi")

    assert(get_fragment("iki [h]iki").try_add_inside(
        get_fragment("[h]iki")).serialized() == "iki hiki hiki")

    assert(get_fragment("iki [h]iki").try_add_inside(
        get_fragment("[h]aka")).serialized() == "iki haka hiki")

    assert(get_fragment("iki [h]iki").try_add_inside(
        get_fragment("[h]iki [h]iki")).serialized() == "iki hiki [h]iki hiki")

    assert(get_fragment("ii hiki hii").try_add_inside(
        get_fragment("[h]iki")).serialized() == "ii hiki hiki hii")

    assert(get_fragment("ii hiki hii").try_add_inside(
        get_fragment("[h]aka")) is None)

    assert(get_fragment("ii hiki [h]iki hii").try_add_inside(
        get_fragment("[h]iki")).serialized() == "ii hiki hiki hiki hii")

    assert(get_fragment("ii hiki [h]iki hii").try_add_inside(
        get_fragment("[h]aka")).serialized() == "ii hiki haka hiki hii")

    assert(get_fragment("ii hiki hiki hiki hii").try_add_inside(
        get_fragment("[h]iki")).serialized() == "ii hiki hiki hiki hiki hii")

    assert(get_fragment("ii hiki hiki hiki hii").try_add_inside(
        get_fragment("[h]aka")) is None)

    assert(get_fragment("ha kanuuna kah").try_add_inside(
        get_fragment("[k]anuuna")).serialized() == "ha kanuuna kanuuna kah")

    assert(get_fragment("ha kanuuna [k]anuuna kah").try_add_inside(
        get_fragment(
            "[k]anuuna")).serialized() == "ha kanuuna kanuuna kanuuna kah")

    assert(get_fragment("nolo painia polon").try_add_inside(
        get_fragment("[p]ainia")).serialized() == "nolo painia painia polon")

    assert(get_fragment("nolon olon").try_add_inside(
        get_fragment("nolon")) is None)

    assert(get_fragment("nolon nolon").try_add_inside(
        get_fragment("nolon")).serialized() == "nolon nolon nolon")

    assert(get_fragment("[k]anuuna [k]anuuna").try_add_inside(
        get_fragment("[k]anuuna")).serialized() == "[k]anuuna kanuuna kanuuna")

    assert(get_fragment("onni [h]inno[issa]").try_add_inside(
        get_fragment("[h]issi")).serialized() == "onni hissi hinno[issa]")


def test_palindrome_fragment_serialized():
    max_extra_length = 4

    fragment = get_fragment("anna")
    assert(fragment.is_palindrome)
    assert(fragment.has_palindrome_part)
    assert(not fragment.is_splittable)
    assert(fragment.get_outer_left_word() == "anna")
    assert(fragment.center_word == "anna")
    assert(fragment.get_outer_right_word() == "anna")
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("unna annu")
    assert(fragment.is_palindrome)
    assert(fragment.has_palindrome_part)
    assert(fragment.is_splittable)
    assert(fragment.get_outer_left_word() == "unna")
    assert(fragment.center_word == "unna")
    assert(fragment.get_outer_right_word() == "annu")
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("unna anna annu")
    assert(fragment.has_palindrome_part)
    assert(fragment.is_palindrome)
    assert(not fragment.is_splittable)
    assert(fragment.get_outer_left_word() == "unna")
    assert(fragment.center_word == "anna")
    assert(fragment.get_outer_right_word() == "annu")
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("unna [k]annu")
    assert(fragment.has_palindrome_part)
    assert(fragment.is_palindrome)
    assert(fragment.is_splittable)
    assert(fragment.get_outer_left_word() == "unna")
    assert(fragment.center_word == "kannu")
    assert(fragment.get_outer_right_word() == "kannu")
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("[k]annu unna")
    assert(fragment.has_palindrome_part)
    assert(not fragment.is_palindrome)
    assert(fragment.is_splittable)
    assert(fragment.get_outer_left_word() == "kannu")
    assert(fragment.center_word == "kannu")
    assert(fragment.get_outer_right_word() == "unna")
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("oli ilo[a]")
    assert(fragment.has_palindrome_part)
    assert(not fragment.is_palindrome)
    assert(fragment.is_splittable)
    assert(fragment.get_outer_left_word() == "oli")
    assert(fragment.center_word == "oli")
    assert(fragment.get_outer_right_word() == "iloa")
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))
    assert(fragment.get_left_words() == ["oli"])
    assert(fragment.get_right_words() == ["iloa"])

    fragment = get_fragment("aam[en] maa")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "aamen")
    assert(fragment.center_word == "aamen")
    assert(fragment.get_outer_right_word() == "maa")
    assert(fragment.serialized() == "aam[en] maa")
    assert(not fragment.is_palindrome)
    assert(fragment.is_splittable)
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("aamen nout[aja] tuonne maa")
    assert(fragment.has_palindrome_part)
    assert(fragment.serialized() == "aamen nout[aja] tuonne maa")
    assert(fragment.get_outer_left_word() == "aamen")
    assert(fragment.center_word == "noutaja")
    assert(fragment.get_outer_right_word() == "maa")
    assert(fragment.get_left_words() == ["aamen", "noutaja"])
    assert(fragment.get_right_words() == ["tuonne", "maa"])
    assert(fragment.is_palindrome)
    assert(fragment.is_splittable)
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("aamen nuti lei [k]ielitunne maa")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "aamen")
    assert(fragment.center_word == "kielitunne")
    assert(fragment.get_outer_right_word() == "maa")
    assert(fragment.get_left_words() == ["aamen", "nuti", "lei"])
    assert(fragment.get_right_words() == ["kielitunne", "maa"])
    assert(fragment.is_palindrome)
    assert(fragment.is_splittable)
    assert(fragment == PalindromeFragment.from_serialized(
        fragment.serialized(), max_extra_length))

    fragment = get_fragment("ajon [a]noja")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "ajon")
    assert(fragment.center_word == "anoja")
    assert(fragment.get_outer_right_word() == "anoja")
    assert(fragment.get_left_words() == ["ajon"])
    assert(fragment.get_right_words() == ["anoja"])
    assert(fragment.is_splittable)

    fragment = get_fragment("ah ha[i]")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "ah")
    assert(fragment.center_word == "ah")
    assert(fragment.get_outer_right_word() == "hai")
    assert(fragment.get_left_words() == ["ah"])
    assert(fragment.get_right_words() == ["hai"])
    assert(fragment.is_splittable)

    fragment = get_fragment("ajon [sa]noja")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "ajon")
    assert(fragment.center_word == "sanoja")
    assert(fragment.get_outer_right_word() == "sanoja")
    assert(fragment.get_left_words() == ["ajon"])
    assert(fragment.get_right_words() == ["sanoja"])
    assert(fragment.is_splittable)

    fragment = get_fragment("[a]iro ori")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "airo")
    assert(fragment.center_word == "airo")
    assert(fragment.get_outer_right_word() == "ori")
    assert(fragment.get_left_words() == ["airo"])
    assert(fragment.get_right_words() == ["ori"])
    assert(fragment.is_splittable)

    fragment = get_fragment("sala [p]alas[i]")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "sala")
    assert(fragment.center_word == "palasi")
    assert(fragment.get_outer_right_word() == "palasi")
    assert(fragment.get_left_words() == ["sala"])
    assert(fragment.get_right_words() == ["palasi"])
    assert(fragment.is_splittable)

    fragment = get_fragment("[a]iro ajon [sa]noja ori")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "airo")
    assert(fragment.center_word == "sanoja")
    assert(fragment.get_outer_right_word() == "ori")
    assert(fragment.get_left_words() == ["airo", "ajon"])
    assert(fragment.get_right_words() == ["sanoja", "ori"])
    assert(fragment.is_splittable)

    fragment = get_fragment("[a]iro ajon [a]noja ori")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "airo")
    assert(fragment.center_word == "anoja")
    assert(fragment.get_outer_right_word() == "ori")
    assert(fragment.get_left_words() == ["airo", "ajon"])
    assert(fragment.get_right_words() == ["anoja", "ori"])
    assert(fragment.is_splittable)

    fragment = get_fragment("onni [h]inno[issa]")
    assert(fragment.has_palindrome_part)
    assert(fragment.get_outer_left_word() == "onni")
    assert(fragment.center_word == "hinnoissa")
    assert(fragment.get_outer_right_word() == "hinnoissa")
    assert(fragment.get_left_words() == ["onni"])
    assert(fragment.get_right_words() == ["hinnoissa"])
    assert(fragment.is_splittable)

    fragment = get_fragment("[air]o [k]o[nna]")
    assert(fragment.has_palindrome_part)
    assert(fragment.is_splittable)
    assert(fragment.get_outer_left_word() == "airo")
    assert(fragment.center_word == "konna")
    assert(fragment.get_outer_right_word() == "konna")
    assert(fragment.get_left_words() == ["airo"])
    assert(fragment.get_right_words() == ["konna"])

    fragment = get_fragment("anna utti")
    assert(not fragment.has_palindrome_part)
    assert(not fragment.is_splittable)
    assert(not fragment.is_palindrome)
    assert(fragment.get_outer_left_word() == "anna")
    assert(fragment.center_word == "anna")
    assert(fragment.get_outer_right_word() == "utti")
    assert(fragment.get_left_words() == ["anna"])
    assert(fragment.get_right_words() == ["utti"])

    fragment = get_fragment("sanna luutii")
    assert(not fragment.has_palindrome_part)
    assert(not fragment.is_splittable)
    assert(not fragment.is_palindrome)
    assert(fragment.get_outer_left_word() == "sanna")
    assert(fragment.center_word == "luutii")
    assert(fragment.get_outer_right_word() == "luutii")
    assert(fragment.get_left_words() == ["sanna"])
    assert(fragment.get_right_words() == ["luutii"])

    fragment = get_fragment("sanna anna luutii")
    assert(not fragment.has_palindrome_part)
    assert(not fragment.is_splittable)
    assert(not fragment.is_palindrome)
    assert(fragment.get_outer_left_word() == "sanna")
    assert(fragment.center_word == "anna")
    assert(fragment.get_outer_right_word() == "luutii")
    assert(fragment.get_left_words() == ["sanna", "anna"])
    assert(fragment.get_right_words() == ["luutii"])


def test_is_character_palindrome():
    assert(is_character_palindrome(""))
    assert(is_character_palindrome("a"))
    assert(is_character_palindrome("aa"))
    assert(is_character_palindrome("ab") is False)
    assert(is_character_palindrome("aaa"))
    assert(is_character_palindrome("aba"))
    assert(is_character_palindrome("abc") is False)


def test_is_splittable_palindrome():
    assert(is_splittable_palindrome(None) is False)
    assert(is_splittable_palindrome("") is False)
    assert(is_splittable_palindrome("abc") is False)
    assert(is_splittable_palindrome("alla") is False)
    assert(is_splittable_palindrome("ollapa pallo") is False)
    assert(is_splittable_palindrome("alla alla alla") is False)

    assert(is_splittable_palindrome("alla alla"))
    assert(is_splittable_palindrome("arava avara"))
    assert(is_splittable_palindrome("atte kumi imuketta"))


def test_find_single_word_palindromes():
    assert(set(find_single_word_palindromes(
        ["otto", "iki", "aja", "hiki"])) ==
        {"otto", "aja", "iki"})


def test_find_single_word_fragments():
    assert(set(find_single_word_fragments(
        ["kanuuna", "hoopo", "annalle", "anna"])) ==
        {"[k]anuuna", "[ho]opo", "anna[lle]", "anna"})


def test_find_multi_word_palindromes():
    assert(len(find_multi_word_palindromes(
        ["seimi"])) == 0)
    assert(len(find_multi_word_palindromes(
        ["sikisi"])) == 0)
    assert(len(find_multi_word_palindromes(
        ["trap"])) == 0)
    assert(len(find_multi_word_palindromes(
        ["ostaja, ostajat"])) == 0)
    assert(len(find_multi_word_palindromes(
        ["fraseologia", "fraseologian"])) == 0)
    assert(len(find_multi_word_palindromes(
        ["säile", "työt", "työteliäs"])) == 0)
    assert(len(find_multi_word_palindromes(
        ["alemma", "ma", "mela", "amme", "me"])) == 0)

    assert(set(find_multi_word_palindromes(
        ["ollapa", "pallo"])) == {"ollapa pallo"})
    assert(set(find_multi_word_palindromes(
        ["avara", "harava"])) == {"avara harava"})
    assert(set(find_multi_word_palindromes(
        ["neulo", "taas", "niin", "saat", "oluen"])) ==
        {"neulo oluen", "oluen neulo", "saat taas", "taas saat"})
    assert(set(find_multi_word_palindromes(
        ["alla", "jo", "ojalla"])) == {"alla jo ojalla", "ojalla jo"})
    assert(set(find_multi_word_palindromes(
        ["alla", "jo", "pahna", "vanhapojalla"])) ==
        {"alla jo pahna vanhapojalla"})
    assert(set(find_multi_word_palindromes(
        ["aira", "laahasi", "kisahaalaria"])) == {"aira laahasi kisahaalaria"})
    assert(set(find_multi_word_palindromes(
        ["saksa", "kaskas"])) == {"saksa kaskas"})
    assert(set(find_multi_word_palindromes(
        ["risoiko", "tokio", "sir"])) == {"risoiko tokio sir"})
    assert(set(find_multi_word_palindromes(
        ["atte", "kumi", "imuketta"])) ==
        {"atte kumi imuketta", "imuketta atte kumi"})
    assert(set(find_multi_word_palindromes(
        ["orava", "varo"])) == {"orava varo"})
    assert(set(find_multi_word_palindromes(
        ["tupakka", "iso", "putosi"])) == {"iso tupakka putosi"})
    assert(set(find_multi_word_palindromes(
        ["tee", "rokka", "venetsialaisten", "evakkoreet"])) ==
        {"tee rokka venetsialaisten evakkoreet"})
    assert(set(find_multi_word_palindromes(
        ["älä", "reputa", "tatu", "perälä"])) == {"älä reputa tatu perälä"})
    assert(set(find_multi_word_palindromes(
        ["sutilla", "hajota", "pato", "ja", "hallitus"])) ==
        {"sutilla hallitus", "sutilla hajota pato ja hallitus"})
    assert(set(find_multi_word_palindromes(
        ["noki", "suu", "mikko", "rokki", "muusikon"])) ==
        {"noki suu muusikon", "noki suu mikko rokki muusikon"})

    # questionable repetition
    assert(set(find_multi_word_palindromes(
        ["iki", "hiki"])) == {"iki hiki", "iki hiki hiki"})
    assert(set(find_multi_word_palindromes(
        ["kauppias", "kivi", "saippua"])) ==
        {"saippua kauppias", "saippua kivi kauppias",
         "saippua kivi kivi kauppias"})

    # repetition would be needed here
    # assert(set(find_multi_word_palindromes(
    #    ["saksa", "kas"])) == {"saksakaskas"})

    assert(set(find_multi_word_palindromes(
        ["aula", "valua"])) == {"aula valua"})
    assert(set(find_multi_word_palindromes(
        ["nupinaula", "aula", "valua", "nipun"])) ==
        {"aula valua", "nupinaula valua nipun",
         "aula valua nipun nupinaula valua"})
    assert(set(find_multi_word_palindromes(
        ["arava", "suola", "suolat", "tavara", "avara", "talous"])) ==
        {"arava tavara", "arava avara", "avara arava", "suola talous",
         "suola tavara arava talous", "suolat talous", "talous suolat",
         "arava talous suola tavara"})
    assert(set(find_multi_word_palindromes(
        ["tuhooja", "jo", "ohut", "oja"])) ==
        {"tuhooja jo ohut", "oja jo", "oja jo ohut tuhooja jo"})
    assert(set(find_multi_word_palindromes(
        ["ennen", "nenä", "ne"])) == {"ennen ne"})

    assert(set(find_multi_word_palindromes(
        ["noki", "suu", "mikko", "raataa", "kanuuna",
         "kaataa", "rokki", "muusikon"])) == {
        "noki suu muusikon",
        "noki suu mikko rokki muusikon",
        "noki suu mikko raataa rokki muusikon",
        "noki suu mikko raataa raataa rokki muusikon",
        "noki suu mikko raataa kaataa rokki muusikon",
        "noki suu mikko raataa kanuuna kaataa rokki muusikon",
        "noki suu mikko raataa kanuuna kanuuna kaataa rokki muusikon",
        "noki suu mikko raataa kaataa kaataa rokki muusikon"})
    assert(set(find_multi_word_palindromes(
        ["tomaatti", "mehu", "paja", "puhe", "mittaamot"])) == {
        "tomaatti mittaamot",
        "tomaatti mehu puhe mittaamot",
        "tomaatti mehu paja puhe mittaamot",
        "tomaatti mehu paja paja puhe mittaamot"})


def get_fragment_texts(words):
    miner = PalindromeFragmentMiner(words, words, False)
    miner.find_multi_word_fragments(3)
    result = set()
    for fragment in miner.fragments:
        result.add(fragment.serialized())
    return result


def test_mine_fragments():
    assert(get_fragment_texts(["ei", "ien"]) ==
           {"ei ie[n]", "ie[n] ei"})
    assert(get_fragment_texts(["ilo", "oli"]) ==
           {"ilo oli", "oli ilo"})
    assert(get_fragment_texts(["ilo", "kooli"]) ==
           {"ilo [ko]oli", "[ko]oli ilo"})
    assert(get_fragment_texts(["ei", "sievä"]) ==
           {"ei [s]ie[vä]", "[s]ie[vä] ei"})
    assert(get_fragment_texts(["kukkakaalilaatikko", "aka"]) ==
           {"aka aka"})
    assert(len(get_fragment_texts(["kukkakaalilaatikko", "laaka"])) == 0)
    assert(len(get_fragment_texts(["4Htoiminta", "nimi"])) == 0)
    assert(get_fragment_texts(["akka", "kakkara"]) ==
           {"akka akka", "akka [k]akka[ra]", "[k]akka[ra] akka"})
