#!/usr/bin/python3

import os
from palindrome_fragment import *


def is_character_palindrome(text):
    result = True
    if text:
        for i in range(int(len(text) / 2) + 1):
            if text[i] != text[len(text) - 1 - i]:
                result = False
                break
    return result


def is_single_word_palindrome(words, candidate):
    return candidate in words and is_character_palindrome(candidate)


def is_splittable_palindrome(palindrome):
    result = False
    if palindrome:
        whitespaceless_palindrome = palindrome.replace(" ", "")
        if is_character_palindrome(whitespaceless_palindrome):
            length = len(whitespaceless_palindrome)
            if length % 2 == 0:
                whitespaceless_first_half = \
                    whitespaceless_palindrome[
                        :int(len(whitespaceless_palindrome) / 2)]
                text = palindrome
                while(not text.startswith(whitespaceless_first_half)):
                    text = text.replace(" ", "", 1)
                result = text[len(whitespaceless_first_half)] == " "
    return result


def find_single_word_palindromes(words):
    result = []
    for word in words:
        if is_single_word_palindrome(words, word):
            result.append(word)
    return result


def load_fragments(palindromes_file):
    fragments = list()
    added_lines = set()
    print("Loading fragment texts...")
    if os.path.isfile(palindromes_file):
        with open(palindromes_file, "r", encoding="utf-8") as file:
            for line in file:
                stripped_line = line.strip()
                if stripped_line not in added_lines:
                    added_lines.add(stripped_line)
        print("    done")
    else:
        print("    file not found: " + palindromes_file)

    print("Sorting fragments...")
    added_lines = list(added_lines)
    added_lines.sort()
    print("    done")

    print("Deserializing fragments...")
    for line in added_lines:
        fragments.append(
            PalindromeFragment.from_serialized(line, 4))
    print("    done")

    return fragments


def load_palindromes(palindromes_file):
    palindromes_set = set()
    fragments = load_fragments(palindromes_file)
    for fragment in fragments:
        if fragment.is_palindrome:
            palindromes_set.add(fragment.serialized())

    palindromes_list = sorted(palindromes_set, key=str.casefold)
    return palindromes_list


def load_words(words_file):
    words = []
    print("Loading words...")
    if os.path.isfile(words_file):
        with open(words_file, "r", encoding="utf-8") as file:
            for line in file:
                words.append(line.strip().lower())
        print("    done")
    else:
        print("    file not found: " + words_file)
    return words
