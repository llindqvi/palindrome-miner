#!/usr/bin/python3

from util import *
import palindrome_util
from palindrome_util import *
import string


def find_single_word_fragments(words):
    miner = PalindromeFragmentMiner(words, set(words))
    miner.find_single_word_fragments()
    for fragment in miner.fragments:
        yield fragment.serialized()


class PalindromeFragmentMiner:
    def __init__(self, root_words, words_set, print_output=True):
        self.root_words = root_words
        self.words_set = words_set
        self.fragments = list()
        self.print_output = print_output
        self.all_words_str = "$".join(words_set)

    def find_fragments(self, extra_max_length):
        self.find_single_word_fragments(extra_max_length)
        self.find_multi_word_fragments(extra_max_length)

    def find_single_word_fragments(self, extra_max_length=3):
        for word in self.root_words:
            self.get_single_word_fragment(word, extra_max_length)

    def get_single_word_fragment(self, word, extra_max_length):
        word_length = len(word)
        reversed_word = word[::-1]
        fragment = None
        for i in range(extra_max_length + 1):
            if word.endswith(word[i:][::-1]):
                extra = word[:i]
                fragment = PalindromeFragment(
                    [word],
                    extra,
                    "",
                    "",
                    "")
                break
            if i > 0 and word.startswith(word[::-1][i:]):
                extra = word[-i:]
                fragment = PalindromeFragment(
                    [word],
                    "",
                    "",
                    "",
                    extra)
                break
        if fragment:
            if self.print_output:
                output_line(fragment.serialized())
            self.fragments.append(fragment)

    def find_multi_word_fragments(self, extra_max_length=3):
        word_count = len(self.root_words)
        index = 0
        for word in self.root_words:
            self.find_fragments_starting_with(word, extra_max_length)
            index += 1
            if index % 100 == 0:
                print(str(index) + " / " + str(word_count))

    def find_fragments_starting_with(self, word, extra_max_length):
        word_length = len(word)
        reversed_word = word[::-1]

        if reversed_word in self.all_words_str:
            for dictionary_word in self.words_set:
                fragment1 = None
                fragment2 = None

                outer_left_extra = ""
                inner_left_extra = ""
                inner_right_extra = ""
                outer_right_extra = ""

                if reversed_word == dictionary_word:
                    self.add_fragment(
                        PalindromeFragment(
                            [word, dictionary_word],
                            "",
                            "",
                            "",
                            ""))
                else:
                    if reversed_word in dictionary_word:
                        parts = dictionary_word.rsplit(reversed_word, 1)
                        if len(parts[0]) <= extra_max_length and \
                                len(parts[1]) <= extra_max_length:
                            inner_right_extra = parts[0]
                            outer_right_extra = parts[1]
                            self.add_fragment(
                                PalindromeFragment(
                                    [word, dictionary_word],
                                    "",
                                    "",
                                    inner_right_extra,
                                    outer_right_extra))
                            self.add_fragment(
                                PalindromeFragment(
                                    [dictionary_word, word],
                                    inner_right_extra,
                                    outer_right_extra,
                                    "",
                                    ""))

    def add_fragment(self, fragment):
        if self.print_output:
            output_line(fragment.serialized())
        self.fragments.append(fragment)


class PalindromeFragmentExtras:
    def __init__(self):
        self.outer_left_extra = ""
        self.inner_left_extra = ""
        self.inner_right_extra = ""
        self.outer_right_extra = ""


class PalindromeFragment:
    def __init__(
        self, words, outer_left_extra, inner_left_extra,
            inner_right_extra, outer_right_extra):
        self.words = words
        self.words_length = len(words)
        self.outer_left_extra = outer_left_extra
        self.inner_left_extra = inner_left_extra
        self.inner_right_extra = inner_right_extra
        self.outer_right_extra = outer_right_extra
        self.calculate_center_word_index()
        self.calculate_is_palindrome()
        self.calculate_is_splittable()
        assert(not self.is_palindrome or self.has_palindrome_part)

        for word in words:
            assert("[" not in word)

        assert(
            len(self.inner_left_extra) == 0 or
            len(self.inner_right_extra) == 0,
            "Inner extra must be on either left or right, not both")

    @staticmethod
    def from_serialized(text, max_extra_length):
        cleaned_str = text.replace("[", "").replace("]", "")
        whitespaceless, word_boundaries = \
            PalindromeFragment.as_word_boundaries(
                cleaned_str)
        words = cleaned_str.split()

        extras = PalindromeFragmentExtras()

        if "[" in text:
            extras = PalindromeFragment.extract_annotated_extras(text)
        else:
            extras = PalindromeFragment.extract_extras(
                    max_extra_length,
                    whitespaceless,
                    cleaned_str,
                    word_boundaries)

        return PalindromeFragment(
            words,
            extras.outer_left_extra,
            extras.inner_left_extra,
            extras.inner_right_extra,
            extras.outer_right_extra)

    @staticmethod
    def extract_extras(
            max_extra_length, whitespaceless, cleaned_str, word_boundaries):
        extras = PalindromeFragmentExtras()
        first_word_length = word_boundaries[0] \
            if len(word_boundaries) > 0 else len(cleaned_str)
        last_word_length = (len(whitespaceless) - word_boundaries[-1]) \
            if len(word_boundaries) > 0 else len(cleaned_str)
        for u in range(min(last_word_length, max_extra_length + 1)):
            for i in range(min(first_word_length, max_extra_length + 1)):
                if u == 0:
                    candidate = whitespaceless[i:]
                else:
                    candidate = whitespaceless[i:-u]

                candidate_word_boundaries = word_boundaries.copy()
                if i > 0:
                    for list_index in range(len(word_boundaries)):
                        candidate_word_boundaries[list_index] -= i

                candidate_inner_left_extra, candidate_inner_right_extra = \
                    PalindromeFragment.extract_inner_extras(
                        max_extra_length,
                        candidate,
                        candidate_word_boundaries)
                if candidate_inner_left_extra or \
                        candidate_inner_right_extra:
                    extras.inner_left_extra = candidate_inner_left_extra
                    extras.inner_right_extra = candidate_inner_right_extra
                    if i > 0:
                        extras.outer_left_extra = whitespaceless[:i]
                    if u > 0:
                        extras.outer_right_extra = whitespaceless[-u:]
                    break

                if palindrome_util.is_character_palindrome(
                        candidate):
                    if i > 0:
                        extras.outer_left_extra = whitespaceless[:i]
                    if u > 0:
                        extras.outer_right_extra = whitespaceless[-u:]
                    break
            else:
                continue  # only executed if the inner loop did NOT break
            break  # only executed if the inner loop DID break
        return extras

    @staticmethod
    def extract_annotated_extras(text):
        extras = PalindromeFragmentExtras()
        if text.startswith("["):
            extras.outer_left_extra = text[1:].split("]", 1)[0]

        if text.endswith("]"):
            extras.outer_right_extra = text[:-1].split("[")[-1]

        inner_right_split = text.split(" [", 1)
        if len(inner_right_split) > 1:
            extras.inner_right_extra = inner_right_split[1].split("]")[0]

        inner_left_split = text.split("] ", 1)
        if len(inner_left_split) > 1:
            extras.inner_left_extra = inner_left_split[0].rsplit("[", 1)[1]
        return extras

    @staticmethod
    def as_word_boundaries(text):
        whitespaceless = replace_any(
            text, " \t" + string.punctuation, "").lower()
        word_boundaries = []
        index = 0
        previous_is_whitespace = False
        for c in text:
            if c in string.punctuation:
                pass
            elif c.isspace():
                if not previous_is_whitespace:
                    word_boundaries.append(index)
                    previous_is_whitespace = True
            else:
                previous_is_whitespace = False
                index += 1

        return whitespaceless, word_boundaries

    @staticmethod
    def extract_inner_extras(
            max_extra_length, whitespaceless, word_boundaries):
        extra = ""
        for i in range(1, max_extra_length + 1):
            center_index = int((len(whitespaceless) - i) / 2)

            left_boundary_present = False
            right_boundary_present = False
            if center_index in word_boundaries:
                left_boundary_present = True
            if center_index + i in word_boundaries:
                right_boundary_present = True

            if (left_boundary_present or right_boundary_present) \
                    and not (left_boundary_present == right_boundary_present):
                candidate = whitespaceless[:center_index] + \
                    whitespaceless[center_index + i:]
                if len(candidate) % 2 == 0 and \
                        palindrome_util.is_character_palindrome(candidate):
                    extra = \
                        whitespaceless[center_index:center_index + i]
                    break

        inner_right_extra = ""
        inner_left_extra = ""
        if extra:
            if left_boundary_present:
                inner_right_extra = extra
            elif right_boundary_present:
                inner_left_extra = extra

        return inner_left_extra, inner_right_extra

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.serialized() == other.serialized() and \
                self.words == other.words
        else:
            return False

    def calculate_is_palindrome(self):
        self.is_palindrome = palindrome_util.is_character_palindrome(
            "".join(self.words).lower())

    def calculate_is_splittable(self):
        words_without_extra = list(self.words_without_extra())
        self.has_palindrome_part = palindrome_util.is_character_palindrome(
            "".join(words_without_extra))
        self.is_splittable = palindrome_util.is_splittable_palindrome(
            " ".join(words_without_extra)) \
            if self.has_palindrome_part else False

    def can_add_palindrome_inside(self):
        return self.is_splittable and \
            not self.inner_left_extra and \
            not self.inner_right_extra

    def can_add_palindrome_around(self):
        return not self.outer_left_extra and not self.outer_right_extra

    def serialized(self):
        return " ".join(self.serialized_words())

    def words_without_extra(self):
        center_word_index = self.center_word_index
        word_index = 0
        for word in self.words:
            word_to_return = word
            if word_index == 0 and self.outer_left_extra:
                word_to_return = word_to_return[len(self.outer_left_extra):]
            if word_index == len(self.words) - 1 and self.outer_right_extra:
                word_to_return = word_to_return[
                    :len(word_to_return) - len(self.outer_right_extra)]
            if word_index == center_word_index and \
                    (self.inner_right_extra or self.inner_left_extra):
                right_extra = self.inner_right_extra \
                    if word_index > 0 else self.outer_left_extra
                left_extra = self.inner_left_extra \
                    if word_index < len(self.words) - 1 else \
                    self.outer_right_extra
                word_to_return = word[
                    len(right_extra):][
                        :len(word) -
                        len(right_extra) -
                        len(left_extra)]
            yield word_to_return
            word_index += 1

    def serialized_words(self):
        center_word_index = self.center_word_index
        word_index = 0
        for word in self.words:
            word_to_yield = word
            if word_index == 0 and self.outer_left_extra:
                word_to_yield = \
                    "[" + self.outer_left_extra + "]" + \
                    word_to_yield[len(self.outer_left_extra):]

            if word_index == self.words_length - 1 and self.outer_right_extra:
                word_to_yield = \
                    word_to_yield[
                        :len(word_to_yield) -
                        len(self.outer_right_extra)] + \
                    "[" + self.outer_right_extra + "]"

            if word_index == center_word_index and \
                    (self.inner_right_extra or self.inner_left_extra):
                word_to_yield = (
                    ("[" + self.inner_right_extra + "]"
                        if self.inner_right_extra else "")) + \
                    word_to_yield[
                        len(self.inner_right_extra):][
                            :len(word_to_yield) - len(self.inner_right_extra) -
                            len(self.inner_left_extra)] + \
                    (("[" + self.inner_left_extra + "]"
                        if self.inner_left_extra else ""))
            yield word_to_yield
            word_index += 1

    def get_outer_left_word(self):
        return self.words[0]

    def get_outer_right_word(self):
        return self.words[-1]

    def calculate_center_word_index(self):
        whitespaceless_palindrome = "".join(self.words)
        if self.outer_left_extra:
            whitespaceless_palindrome = whitespaceless_palindrome[
                len(self.outer_left_extra):]
        if self.outer_right_extra:
            whitespaceless_palindrome = whitespaceless_palindrome[
                :-len(self.outer_right_extra)]
        whitespaceless_palindrome_length = len(whitespaceless_palindrome)

        center_index = whitespaceless_palindrome_length / 2

        index = 0
        center_word_index = -1
        word_index = 0
        for word in self.words:
            index += len(word)
            if word_index == 0:
                index -= len(self.outer_left_extra)
            if index == center_index:
                if whitespaceless_palindrome_length % 2 == 1:
                    if len(self.inner_left_extra) > \
                         len(self.inner_right_extra):
                        center_word_index = word_index
                    else:
                        center_word_index = word_index + 1
                else:
                    if len(self.inner_left_extra) >= \
                         len(self.inner_right_extra):
                        center_word_index = word_index
                    else:
                        center_word_index = word_index + 1
                break
            elif index > center_index:
                center_word_index = word_index
                break
            word_index += 1
        self.center_word_index = center_word_index
        self.center_word = self.words[self.center_word_index] \
            if self.center_word_index >= 0 else None

    def try_add_inside(self, fragment):
        result = None
        if self.is_splittable:
            if self.can_add_palindrome_inside() and \
                    fragment.can_add_palindrome_around():
                result = PalindromeFragment(
                    self.get_left_words() +
                    fragment.words +
                    self.get_right_words(),
                    self.outer_left_extra,
                    fragment.inner_left_extra,
                    fragment.inner_right_extra,
                    self.outer_right_extra)
            elif self.inner_right_extra == fragment.outer_left_extra[::-1] \
                and not self.inner_left_extra and \
                    not fragment.outer_right_extra:
                result = PalindromeFragment(
                    self.get_left_words() +
                    fragment.words +
                    self.get_right_words(),
                    self.outer_left_extra,
                    fragment.inner_left_extra,
                    fragment.inner_right_extra,
                    self.outer_right_extra)
            elif self.inner_left_extra == fragment.outer_right_extra[::-1] \
                and not self.inner_right_extra and \
                    not fragment.outer_left_extra:
                result = PalindromeFragment(
                    self.get_left_words() +
                    fragment.words +
                    self.get_right_words(),
                    self.outer_left_extra,
                    fragment.inner_left_extra,
                    fragment.inner_right_extra,
                    self.outer_right_extra)

        if result is None:
            if self.center_word and self.center_word == fragment.user_str():
                result = PalindromeFragment(
                    self.get_left_words() +
                    [self.center_word] +
                    self.get_right_words(),
                    self.outer_left_extra,
                    "",
                    "",
                    self.outer_right_extra)
                if not palindrome_util.is_character_palindrome(
                        "".join(result.words_without_extra())):
                    result = None
        return result

    def try_add_outside(self, fragment):
        return fragment.try_add_inside(self)

    def get_last_left_word_index(self):
        index = self.center_word_index
        if (len(self.inner_right_extra) > 0 or len(self.inner_left_extra) > 0):
            if len(self.inner_right_extra) > len(self.inner_left_extra):
                index -= 1
        elif index > 0 and \
                (len("".join(self.words[:index])) >
                 len("".join(self.words[index + 1:]))):
            index -= 1
        return index

    def get_left_words(self):
        return self.words[:self.get_last_left_word_index() + 1]

    def get_right_words(self):
        return self.words[self.get_last_left_word_index() + 1:]

    def user_str(self):
        return " ".join(self.words)

    def __str__(self):
        return "Fragment:" + self.serialized()
