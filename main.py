#!/usr/bin/python3

import os
import sys
import argparse
import readchar  # pip install readchar
from pyautogui import typewrite  # pip install pyautogui
import test_palindromes
from util import *
from palindrome_util import *
from xml.dom import minidom
from palindrome_fragment import *
from palindrome_miner import *


def get_root_words(args, words_set):
    if args.root_words:
        if args.root_words.isdigit():
            words_list = list(words_set)
            words_list.sort()
            root_words = \
                words_list[int(args.root_words):int(args.root_words)*2]
        elif "." in args.root_words:
            root_words = load_words(args.root_words)
        else:
            root_words = list()
            for word in args.root_words.split(","):
                trimmed_word = word.strip()
                if trimmed_word in words_set:
                    root_words.append(trimmed_word)
        root_words.sort()
    else:
        root_words = list(words_set)
        root_words.sort()
        if args.start_word:
            root_words = root_words[root_words.index(args.start_word):]

    return root_words


def mine_fragments(args):
    words = load_words(args.words_file)
    words_set = set(words)
    root_words = get_root_words(args, words_set)

    miner = PalindromeFragmentMiner(root_words, words_set)
    miner.find_fragments(4)


def mine(args):
    words = load_words(args.words_file)
    words_set = set(words)

    palindromes_set = set(load_palindromes(args.palindromes_file))

    # print("Finding single-word palindromes...")
    # single_word_palindromes = find_single_word_palindromes(words_set)
    # single_word_palindromes.sort()
    # for word in single_word_palindromes:
    #     if not word in palindromes_set:
    #         output_line(word)

    print("Finding multi-word palindromes...")
    root_words = get_root_words(args, words_set)

    miner = PalindromeMiner(
        args.max_word_count, root_words, words_set, palindromes_set)
    new_palindromes = miner.find_multi_word_palindromes_with_max_depth()

    # new_palindromes = find_multi_word_palindromes_with_max_depth(
    #     root_words, words_set, max_depth, palindromes_set)
    new_palindromes.sort()
    print("New multi-word palindromes:")
    for word in new_palindromes:
        print(word)


def print_palindromes(args):
    palindromes = load_palindromes(args.palindromes_file)
    truncate_output()
    if args.output:
        for palindrome in palindromes:
            output_line(palindrome, False)
    else:
        for palindrome in palindromes:
            print(
                palindrome +
                (" (splittable)"
                    if is_splittable_palindrome(palindrome)
                    else ""))


def search_fragments(cased_phrase, fragments):
    has_initial_phrase = cased_phrase is not None
    if not cased_phrase:
        print("Search phrase:")
        cased_phrase = input()

    while cased_phrase and cased_phrase != "exit()":
        matches = list()
        phrase = cased_phrase.lower()

        input_fragment = PalindromeFragment.from_serialized(
                cased_phrase, 4)
        input_fragment_serialized = input_fragment.serialized()
        print("[" + ("x" if input_fragment.is_palindrome else " ") +
              "] palindrome")
        print("[" + ("x"
              if input_fragment.has_palindrome_part and
              not input_fragment.is_palindrome else " ") +
              "] near-palindrome")
        print("[" + ("x" if input_fragment.is_splittable else " ") +
              "] splittable")

        if input_fragment_serialized != cased_phrase:
            print("annotated: " + input_fragment_serialized)
        print()

        matches.append(input_fragment)
        match_count = 1
        for fragment in fragments:
            fragment_str = fragment.user_str().lower()
            whitespaceless = fragment_str.replace(" ", "")
            if phrase in fragment_str or \
                    phrase in whitespaceless:
                match_count += 1
                matches.append(fragment)

        if input_fragment:
            any_around = input_fragment.can_add_palindrome_around()
            any_inside = input_fragment.can_add_palindrome_inside()

            for fragment in fragments:
                if not any_inside:
                    new = input_fragment.try_add_inside(fragment)
                    if new:
                        matches.append(new)

                if not any_around:
                    new = input_fragment.try_add_outside(fragment)
                    if new:
                        matches.append(new)

        matches.sort(
            key=lambda x: palindrome_util.edit_distance(
                input_fragment_serialized, x.serialized()))

        index = 0
        inputchar = b' '
        for fragment in matches:
            serialized = fragment.serialized()
            print(
                (str(index + 1) + ": " + serialized +
                 ("\t<- palindrome" if fragment.is_palindrome else "")).
                expandtabs(64))
            index += 1
            if index > 0 and index % 100 == 0:
                print("--More--")
                inputchar = readchar.readchar()
                if inputchar != b' ':
                    break
        inputchar = inputchar.decode("utf-8").strip()

        if has_initial_phrase:
            break
        print("New search phrase:")
        typewrite(inputchar)
        cased_phrase = input("")
        cased_phrase = cased_phrase.replace(
                "[", "").replace("]", "")
        if cased_phrase.isdigit() and int(cased_phrase) <= len(matches):
            cased_phrase = matches[int(cased_phrase) - 1].user_str()
            print(cased_phrase)


def print_fragments(args):
    fragments = load_fragments(args.palindromes_file)
    truncate_output()
    if args.output:
        for fragment in fragments:
            output_line(fragment.serialized(), False)
    else:
        for fragment in fragments:
            print(
                fragment.serialized() +
                (" (splittable)"
                    if fragment.is_splittable
                    else ""))


def main():
    parser = argparse.ArgumentParser(description='Palindrome miner')
    parser.add_argument(
        'command', nargs=1,
        choices=[
            "test",
            "mine",
            "mine_fragments",
            "list",
            "list_fragments",
            "compose"],
        help='The command to run')
    parser.add_argument(
        '-o', '--output', nargs='?', type=str, help='Output file')
    parser.add_argument(
        '--words_file', nargs='?', type=str,
        help='File containing words to use', default='words.txt')
    parser.add_argument(
        '--palindromes_file', nargs='?', type=str,
        help='File containing initial set of palindromes',
        default='palindromes.txt')
    parser.add_argument(
        '--root_words', nargs='?', type=str,
        help='Used to limit mining set', default='')
    parser.add_argument(
        '--max_word_count', nargs='?', type=int,
        help='Maximum number of words in mined palindromes', default=4)
    parser.add_argument(
        '--start_word', nargs='?', type=str,
        help='Word to start mining from', default=None)
    parser.add_argument(
        '--search_phrase', nargs='?', type=str,
        help='Search phrase using which to search palindromes or fragments',
        default=None)
    args = parser.parse_args()

    open_output(args.output, "a+")

    if "test" in args.command:
        test_palindromes.test()
    if "mine" in args.command:
        mine(args)
    if "mine_fragments" in args.command:
        mine_fragments(args)
    if "list" in args.command:
        print_palindromes(args)
    if "list_fragments" in args.command:
        print_fragments(args)
    if "compose" in args.command:
        fragments = load_fragments(args.palindromes_file)
        search_fragments(args.search_phrase, fragments)


if __name__ == "__main__":
    main()
