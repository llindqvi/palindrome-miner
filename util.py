#!/usr/bin/python3

import numpy


debug = False
output_file = None


def rreplace(s, old, new):
    li = s.rsplit(old, 1)
    return new.join(li)


def debug_log(indent_level, text):
    global debug
    if debug:
        print((" " * 2 * indent_level) + text)


def output_line(text, also_to_stdout=True):
    global output_file
    if output_file:
        output_file.write(text + "\n")
        output_file.flush()
    if not output_file or also_to_stdout:
        print(text)


def set_debug(value):
    global debug
    debug = value


def truncate_output():
    global output_file
    if output_file:
        output_file.seek(0, 0)
        output_file.truncate()


def open_output(output_file_name, output_mode="a+", output_encoding="utf-8"):
    if output_file_name:
        global output_file
        output_file = open(
            output_file_name, output_mode, encoding=output_encoding)


def replace_any(text, to_replace_list, new):
    result = text
    for old in to_replace_list:
        if old in result:
            result = result.replace(old, new)
    return result


def edit_distance(text1, text2):
    matrix_height = len(text1) + 1
    matrix_width = len(text2) + 1

    matrix = numpy.zeros((matrix_height, matrix_width))

    for x in range(matrix_height):
        matrix[x, 0] = x
    for y in range(matrix_width):
        matrix[0, y] = y

    for x in range(1, matrix_height):
        for y in range(1, matrix_width):
            if text1[x-1] == text2[y-1]:
                matrix[x, y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1)
            else:
                matrix[x, y] = min(
                    matrix[x - 1, y] + 1,
                    matrix[x-1, y-1] + 1,
                    matrix[x, y-1] + 1)

    return matrix[matrix_height - 1, matrix_width - 1]
