# Backlog
This backlog acts as the requirements specification document for the project. Also tasks derived from the requirements are tracked here in a separate list. 

## Requirements

Epics are identified with E-prefix. Functional epics are numbered <100 and non-function >= 100. Stories belong under epics and identified with S prefix.

- E1 palindrome search
    - S1 [+] find single word palindromes from word list
    - S2 [~] find multi word palindromes from word list
    - S3 [+] continue multi word palindrome search from speficied word
- E2 interactive mode to compose palindromes 
    - S1 [-] user types in text and application shows continuation options resulting in palindrome
    - S2 [-] mode to type in center piece
    - S3 [-] mode to type in splittable palindromes around existing one
    - S4 [-] ui to show palindrome part of the user input
- E3 Finnish language support
    - S1 [+] load Finnish words from kotus 
    - S2 [-] load Finnish words with all possible inflected forms using Voikko instead of own inflection code
    - S3 [+] load Finnish names  
    - S4 [+] load English names  
- E4 palindrome persistent storage
    - S1 [+] sorting and duplicate and non-palindrome removal of palindrome files via command line arguments
    - S2 [+] save found palindromes to persistent storage (file / database / etc.)
    - S3 [+] load palindromes from storage at startup
    - S4 [+] categorize palindromes to splittable vs. center pieces
- E5 palindrome composition system
    - S1 [+] finding of palindrome parts from permutations of words
    - S2 [+] ability to combine compatible parts to form palindromes
    - S3 [+] find and display palindrome parts containing a search phrase
    - S4 [+] find and display combinations of palindrome parts containing a search phrase
- E100 stability
    - S1 [+] Palindrome search is able to run without crashing into kotus word list
- E101 modularity
    - S1 [+] have Finnish word loading in own script that outputs word list file
    - S2 [+] use argparse to enable diffrentation of behavior based in arguments without changing code
    - S3 [+] take words from input file into palindrome miner
    - S4 [+] palindrome miner class to pass less parameters
- E102 testability
    - S1 [+] have test system into which tests can be written
    - S2 [+] have test system that is invokeable using command line parameters
    - S3 [-] use python unit testing framework instead of test method + asserts
    - S4 [-] palindrome search should proceed in same order for same inputs
- E103 performance
    - S1 [+] create vscode launch configuration that measures performance
    - S2 [+] create test set in which to measure performance 
    - S3 [-] profile palindrome finding performance and optimize
    - S4 [+] profiling results are viewable in GUI (e.g. snakeviz)
- E104 maintainability
    - S1 [+] code complies to pep8 style guide
- E200 requirements management
    - S1 [~l learn requirements management as side-product of the project
    - S2 [+] document partial requirements
    - S3 [-] specify level of full requirements documentation
    - S4 [-] reach requirements complete milestone
    - S5 [-] document requirements into use doorstop RM tool

## TASKS

Non-exhaustive list of tasks to pursue next. In rough priority order.

- run finder for long periods and deeper max_depths to catch and fix problems (will assert in found if words and characters do not match). Req: E100.S1
- create acceleration structure for finding words that startwith(...) from words set Req: E103.S3
- add test that has long word first containing reverses of many other words: mirror of case "alla jo pahna vanhapojalla"
- consider assembling center pieces from palindrome fragments that are compatible:
    - aam(en)...maa <-> ...nuti käki tun(ne)...
    - thus for interactive composition also palindrome fragments should be stored into database along with compatibility information
    - fragments can also be derived be existing palindromes by decomposition
- list numbers on continuation options, let user to type free text or number to speed up composition process Req:E2.S1 
- get_fragment to return None if no fragment was detected
- treat user input words as valid words
    - e.g. bogus word: (h)appa would provide continuation with "iki (h)iki": "iki happa hiki"



